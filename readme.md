# Paul Scripts

My pre-college personal website.

![cover](screenshot.png)

## Usage

So far you can run it with:

    php -S localhost:8000

## License

MIT
