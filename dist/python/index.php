<?php
require '../include/antet.php';
afiseazaAntet('Python (Paul Scripts)', '', '');
?>

<div id="titlu">
	<h2 id="python_titlu">Python</h2>
</div>
<div id="scris">

<p>Python este limbajul de programare perfect pentru a scrie programe rapid. De multe ori &icirc;mi pun &icirc;ntreb&#259;ri de genul: c&acirc;te nume de familie se termin&#259; &icirc;n &quot;<a href="../altele/numesiprenume/escu.html" title="sunt cel putin 4349">escu</a>&quot;?, c&acirc;te cifre are  2 la puterea 10000, cum s&#259; fac s&#259; iau toate nick-urile forumi&#351;tilor de pe un sait.</p>
<p>&Icirc;n Python numerele &icirc;ntregi pot avea un numar nelimitat de cifre, operatorul pentru ridicare la putere este <code>**</code>, deci <code>len(str(2**10000))</code> &icirc;ntoarce 3011. C&acirc;te r&acirc;nduri de C++ sau Pascal sunt necesare pentru a &icirc;ntoarce acela&#351;i rezultat?</p>
<p>Scripturi:</p>
<ul class="lista_sc">
	<li><a href="phpbbUsers">phpBB Users</a> (scrie &icirc;ntr-un fi&#351;ier toate nick-urile utilizatorilor de pe un forum phpBB)</li>
	<li><a href="seTerminaIn">Se termin&#259; &icirc;n... </a> (arat&#259; toate denumirile dintr-o list&#259; cu o termina&#355;ie specificat&#259;)</li>
</ul>
</div>
<?php
require '../include/footer.php';
afiseazaFooter();
?>
