<?php
require '../../include/antet.php';
afiseazaAntet('Nume &#351;i prenume (Paul Scripts)', 'Lista cu nume si prenume din Romanaia extrasa de Paul Nechifor', 'Paul Scripts, Paul Nechifor, telefoane, baza de date, nume, prenume, Romania, romanesti');
?>

<div id="titlu">
	<h3 class="t_pagina">Nume &#351;i prenume</h3>
</div>
<div id="scris">

<p><a href="nume.txt">nume.txt</a> (145142 de denumiri - 1314 KiB)<br /><a href="prenume.txt">prenume.txt</a> (41630 de denumiri - 352 KiB)</p>
<p>Numele le-am scos dintr-o baz&#259; de date a unui program de numere de telefoane rom&acirc;ne&#351;ti. Baza de date respectiv&#259; con&#355;inea la nume &#351;i prenume denumirea unor firme. De&#351;i am scos cele mai multe astfel de firme este posibil s&#259; mai fie nume de firme &icirc;n aceste liste. Listele con&#355;in &#351;i foarte multe denumiri str&#259;ine pentru c&#259; nu am cum s&#259; le elimin. </p>
<p>Dac&#259; vrei s&#259; g&#259;se&#351;ti toate denumirele care se termin&#259; &icirc;ntr-un sufix ia scriptul Python &quot;<a href="../../python/seTerminaIn">Se termina &icirc;n...</a>&quot;.</p>
</div>
<?php
require '../../include/footer.php';
afiseazaFooter();
?>
